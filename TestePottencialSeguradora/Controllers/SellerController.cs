﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Pottencial.BusinessEntities;
using Pottencial.BusinessRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestePottencialSeguradora.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SellerController : ControllerBase
    {
        private readonly ILogger<SellerController> _logger;
        SellerBusinessRules SellerBusinessRules = new SellerBusinessRules();

        public SellerController(ILogger<SellerController> logger)
        {
            _logger = logger;
        }

        // POST /Seller/GetSellers
        /// <summary>
        /// Consulta os vendedores, pode ser usado qualquer propriedade para a consulta
        /// </summary>
        /// <remarks>
        /// Exemplo:
        ///
        ///     POST /Todo
        ///     {
        ///        "Id": 1,
        ///        "Name": "Kauan",
        ///        "CpfCnpj": "88510278032",
        ///        "Email": "k.kauanxleto@gmail.com",
        ///        "Phone": "11968613989"
        ///     }
        ///
        /// </remarks>
        /// <param name="value"></param>
        /// <returns> Lista de vendedores cadastrados </returns>
        /// <response code = "200"> Lista de vendedores cadastrados</response>
        /// <response code = "500"> Erro na consulta</response>
        [Route("GetSellers")]
        [HttpPost]
        public ActionResult<IList<Seller>> GetSellers([FromBody]Seller entity = null)
        {
            try
            {
                var sellerInfos = this.SellerBusinessRules.GetSellers(entity);

                return Ok(sellerInfos);
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        // POST /Seller/SaveSeller
        /// <summary>
        /// Cadastra um novo vendedor / Caso informar o (id) vendedor será atualizado, pode ser usado o id do vendedor no cadastro da venda
        /// </summary>
        /// <remarks>
        /// Exemplo:
        ///
        ///     POST /Todo
        ///     {
        ///        "Id": 0,
        ///        "Name": "Kauan",
        ///        "CpfCnpj": "88510278032",
        ///        "Email": "k.kauanxleto@gmail.com",
        ///        "Phone": "11968613989"
        ///     }
        ///
        /// </remarks>
        /// <param name="value"></param>
        /// <returns> Um novo item criado</returns>
        /// <response code = "200" > Retorna o novo item criado ou Atualiza o item se ele ja existir</response>
        /// <response code = "500" > Se o item não for criado</response>
        [Route("SaveSeller")]
        [HttpPost]
        public ActionResult<Seller> SaveSeller([FromBody]Seller entity)
        {
            try
            {
                this.SellerBusinessRules.SaveSeller(entity);

                return Ok(entity);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
