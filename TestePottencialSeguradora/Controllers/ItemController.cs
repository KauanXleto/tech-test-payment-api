﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Pottencial.BusinessEntities;
using Pottencial.BusinessRules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestePottencialSeguradora.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItemController : ControllerBase
    {
        private readonly ILogger<ItemController> _logger;
        ItemBusinessRules ItemBusinessRules = new ItemBusinessRules();

        public ItemController(ILogger<ItemController> logger)
        {
            _logger = logger;
        }

        // POST /Item/GetItems
        /// <summary>
        /// Consulta os itens cadastrados, pode ser usado qualquer propriedade para a consulta
        /// </summary>
        /// <remarks>
        /// Exemplo:
        ///
        ///     POST /Item/GetItems
        ///     {
        ///         "id": 1,
        ///         "name": "Item1",
        ///         "unitValue": 0,
        ///         "quantity": 0
        ///     }
        /// </remarks>
        /// <param name="value"></param>
        /// <returns> Lista de itens cadastrados </returns>
        /// <response code = "200"> Lista de itens cadastrados</response>
        /// <response code = "500"> Erro na consulta</response>
        [Route("GetItems")]
        [HttpPost]
        public ActionResult<IList<Item>> GetItems([FromBody]Item entity = null)
        {
            try
            {
                var ItemInfos = this.ItemBusinessRules.GetItems(entity);

                return Ok(ItemInfos);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        // POST /Item/GetSaveItemItems
        /// <summary>
        /// Cadastra um novo item / Caso informar o (id) item será atualizado, pode ser usado o id do item no cadastro da venda
        /// </summary>
        /// <remarks>
        /// Exemplo:
        ///
        ///     POST /Item/GetSaveItemItems
        ///     {
        ///         "id": 0,
        ///         "name": "Item1",
        ///         "unitValue": 1,
        ///         "quantity": 1
        ///     }
        /// </remarks>
        /// <param name="value"></param>
        /// <returns> Retorna o item que foi cadastrado </returns>
        /// <response code = "200"> Retorna o item que foi cadastrado/Atualizado </response>
        /// <response code = "500"> Erro no cadastro </response>
        [Route("SaveItem")]
        [HttpPost]
        public ActionResult<Item> SaveItem([FromBody]Item entity)
        {
            try
            {
                this.ItemBusinessRules.SaveItem(entity);

                return Ok(entity);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
