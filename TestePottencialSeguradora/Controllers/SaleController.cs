﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Pottencial.BusinessEntities;
using Pottencial.BusinessRules;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace TestePottencialSeguradora.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly ILogger<SaleController> _logger;
        CoreBusinessRules CoreBusinessRules = new CoreBusinessRules();

        public SaleController(ILogger<SaleController> logger)
        {
            _logger = logger;
        }

        // GET /Sale/GetSaleStatus
        /// <summary>
        /// Retorna os status das vendas
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="value"></param>
        /// <returns> 
        /// </returns>
        /// <response code = "200"> Retorna a lista de status</response>
        /// <response code = "500"> Erro no cadastro </response>
        [Route("GetSaleStatus")]
        [HttpGet]
        public ActionResult<IList<SaleStatus>> GetSaleStatus()
        {
            try
            {
                var SaleStatus = this.CoreBusinessRules.GetSaleStatus();

                return Ok(SaleStatus);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }

        // GET /Sale/GetSaleInfo
        /// <summary>
        /// Retorna as vendas realizadas pelo SaleInfoId
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <param name="value"></param>
        /// <returns> 
        /// </returns>
        /// <response code = "200"> Retorna a venda que foi realizada</response>
        /// <response code = "500"> Erro no cadastro </response>
        [Route("GetSaleInfo")]
        [HttpGet]
        public ActionResult<IList<SaleInfo>> GetSaleInfo([FromHeader]int SaleInfoId)
        {
            try
            {
                var SaleInfos = this.CoreBusinessRules.GetSaleInfo(new SaleInfo() { Id = SaleInfoId });

                return Ok(SaleInfos);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        // POST /Sale/RegisterSale
        /// <summary>
        /// Cadastra uma nova venda, não é necessário informar o Id do item nem do vendedor, caso não informado, será cadastrado automaticamente
        /// </summary>
        /// <remarks>
        /// Exemplo:
        ///
        ///     POST /Sale/RegisterSale
        ///     {
        ///       "sellerId": 0,
        ///       "dateSale": "2023-01-18T22:18:44.402Z",
        ///       "items": [
        ///         {
        ///           "id": 0,
        ///           "name": "Item1",
        ///           "unitValue": 1,
        ///           "quantity": 1
        ///         },
        ///         {
        ///           "id": 0,
        ///           "name": "Item2",
        ///           "unitValue": 2,
        ///           "quantity": 2
        ///         }
        ///       ],
        ///       "seller": {
        ///         "id": 0,
        ///         "name": "Kauan",
        ///         "cpfCnpj": "88510278032",
        ///         "email": "k.kauanxleto@gmail.com",
        ///         "phone": "11968613989"
        ///       }
        ///     }
        /// </remarks>
        /// <param name="value"></param>
        /// <returns> Retorna o id e status da venda realizada </returns>
        /// <response code = "200"> Retorna o id e status da venda realizada </response>
        /// <response code = "500"> Erro no cadastro </response>
        [Route("RegisterSale")]
        [HttpPost]
        public ActionResult RegisterSale([FromBody]SaleInfo entity)
        {
            try
            {
                this.CoreBusinessRules.RegisterSale(entity);

                return Ok(new { 
                    Id = entity.Id,
                    Status = "Aguardando pagamento"
                });
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        // POST /Sale/UpdateSaleStatus
        /// <summary>
        /// Atualiza o status da venda
        /// </summary>
        /// <param name="value"></param>
        /// <returns> Retorna se o item foi atualizado com sucesso </returns>
        /// <response code = "200"> Retorna se o item foi atualizado com sucesso </response>
        /// <response code = "500"> Erro na atualização </response>
        [Route("UpdateSaleStatus")]
        [HttpPost]
        public ActionResult<string> UpdateSaleStatus(int SaleId, SaleStatus.ESaleStatus StatusSale)
        {
            try
            {
                this.CoreBusinessRules.UpdateSaleStatus(SaleId, StatusSale);

                return Ok("Atualizado com sucesso!!");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }
    }
}
