﻿using Pottencial.BusinessEntities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pottencial.Provider
{
    public class CoreProvider
    {

        public List<SaleStatus> DBSaleStatus = new List<SaleStatus>() {
            new SaleStatus() { Id = 1, Name = "Aguardando pagamento" },
            new SaleStatus() { Id = 2, Name = "Pagamento aprovado" },
            new SaleStatus() { Id = 3, Name = "Enviado para transportadora" },
            new SaleStatus() { Id = 4, Name = "Entregue" },
            new SaleStatus() { Id = 5, Name = "Cancelada" }
        };

        public static List<SaleInfo> DBSaleInfo { get; set; }

        public IList<SaleStatus> GetSaleStatus()
        {
            var result = new List<SaleStatus>();
            result.AddRange(DBSaleStatus);

            return result;
        }

        public IList<SaleInfo> GetSaleInfo(SaleInfo filter)
        {
            if (DBSaleInfo == null)
                DBSaleInfo = new List<SaleInfo>();

            var result = new List<SaleInfo>();
            result.AddRange(DBSaleInfo);

            if (filter.Id > 0)
                result = result.Where(x => x.Id == filter.Id).ToList();

            if (filter.SellerId > 0)
                result = result.Where(x => x.SellerId == filter.SellerId).ToList();

            if (filter.SaleStatusId > 0)
                result = result.Where(x => x.SaleStatusId == filter.SaleStatusId).ToList();

            if(result != null && result.Count() > 0)
            {
                var Status = this.GetSaleStatus();

                foreach (var item in result)
                {
                    item.SaleStatus = Status.Where(x => x.Id == item.SaleStatusId).FirstOrDefault();
                }
            }

            return result;
        }

        public int InsertSaleInfo(SaleInfo entity)
        {
            if (DBSaleInfo == null)
                DBSaleInfo = new List<SaleInfo>();

            entity.Id = DBSaleInfo.Count() + 1;
            DBSaleInfo.Add(entity);

            return entity.Id;
        }
        public int UpdateSaleInfo(SaleInfo entity)
        {
            if (DBSaleInfo == null)
                DBSaleInfo = new List<SaleInfo>();

            var indexItem = DBSaleInfo.FindIndex(x => x.Id == entity.Id);
            DBSaleInfo[indexItem] = entity;

            return entity.Id;
        }

    }
}
