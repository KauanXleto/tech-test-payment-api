﻿using Pottencial.BusinessEntities;
using Pottencial.Commun.TextHelper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pottencial.Provider
{
    public class ItemProvider
    {
        public static List<Item> DBItem { get; set; }

        public IList<Item> GetItems(Item filter)
        {
            if (DBItem == null)
                DBItem = new List<Item>();

            List<Item> result = new List<Item>();
            result.AddRange(DBItem);

            if (filter.Id > 0)
                result = result.Where(x => x.Id == filter.Id).ToList();

            if (!string.IsNullOrWhiteSpace(filter.Name))
                result = result.Where(x => NormalizeString.StandardizeText(x.Name).Contains(NormalizeString.StandardizeText(filter.Name))).ToList();

            if (filter.Quantity > 0)
                result = result.Where(x => x.Quantity == filter.Quantity).ToList();

            if (filter.UnitValue > 0)
                result = result.Where(x => x.UnitValue == filter.UnitValue).ToList();

            return result;
        }

        public int InsertItem(Item entity)
        {
            if (DBItem == null)
                DBItem = new List<Item>();

            entity.Id = DBItem.Count() + 1;
            DBItem.Add(entity);

            return entity.Id;
        }

        public int UpdateItem(Item entity)
        {
            if (DBItem == null)
                DBItem = new List<Item>();

            var indexItem = DBItem.FindIndex(x => x.Id == entity.Id);
            DBItem[indexItem] = entity;

            return entity.Id;
        }
    }
}
