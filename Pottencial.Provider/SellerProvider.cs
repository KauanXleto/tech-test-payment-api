﻿using Pottencial.BusinessEntities;
using Pottencial.Commun.TextHelper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pottencial.Provider
{
    public class SellerProvider
    {
        public static List<Seller> DBSeller { get; set; }

        public IList<Seller> GetSellers(Seller filter)
        {
            if (DBSeller == null)
                DBSeller = new List<Seller>();

            List<Seller> result = new List<Seller>();
            result.AddRange(DBSeller);

            if (filter.Id > 0)
                result = result.Where(x => x.Id == filter.Id).ToList();

            if (!string.IsNullOrWhiteSpace(filter.Name))
                result = result.Where(x => NormalizeString.StandardizeText(x.Name).Contains(NormalizeString.StandardizeText(filter.Name))).ToList();

            if (!string.IsNullOrWhiteSpace(filter.CpfCnpj))
                result = result.Where(x => NormalizeString.StandardizeText(x.CpfCnpj).Contains(NormalizeString.StandardizeText(filter.CpfCnpj))).ToList();

            if (!string.IsNullOrWhiteSpace(filter.Phone))
                result = result.Where(x => NormalizeString.StandardizeText(x.Phone).Contains(NormalizeString.StandardizeText(filter.Phone))).ToList();

            if (!string.IsNullOrWhiteSpace(filter.Email))
                result = result.Where(x => NormalizeString.StandardizeText(x.Email).Contains(NormalizeString.StandardizeText(filter.Email))).ToList();

            return result;
        }

        public int InsertSeller(Seller entity)
        {
            if (DBSeller == null)
                DBSeller = new List<Seller>();

            entity.Id = DBSeller.Count() + 1;
            DBSeller.Add(entity);

            return entity.Id;
        }

        public int UpdateSeller(Seller entity)
        {
            if (DBSeller == null)
                DBSeller = new List<Seller>();

            var indexSeller = DBSeller.FindIndex(x => x.Id == entity.Id);
            DBSeller[indexSeller] = entity;

            return entity.Id;
        }
    }
}
