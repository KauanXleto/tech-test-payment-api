﻿using Pottencial.BusinessEntities;
using Pottencial.Provider;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pottencial.BusinessRules
{
    public class ItemBusinessRules
    {
        ItemProvider ItemProvider = new ItemProvider();

        public IList<Item> GetItems(Item filter)
        {
            return this.ItemProvider.GetItems(filter);
        }
        public int SaveItem(Item entity, bool? IfExistReturn = false)
        {
            entity.ExecuteValidation();

            if (entity.Id > 0)
            {
                var findItem = this.GetItems(new Item() { Id = entity.Id }).FirstOrDefault();

                if(findItem == null || (findItem != null && findItem.Id == 0))
                    throw new Exception("Erro!! Item não encontrado");


                    this.ItemProvider.UpdateItem(entity);
            }
            else
            {
                //var existSameItem = this.GetItems(new Item()
                //{
                //    Name = entity.Name,
                //    Quantity = entity.Quantity,
                //    UnitValue = entity.UnitValue
                //}).FirstOrDefault();

                //if (existSameItem != null && existSameItem.Id > 0)
                //{
                //    if (IfExistReturn == true)
                //        entity = existSameItem;
                //    else
                //        throw new Exception("Erro!! Já existe um item cadastrado com essa mesma configuração");
                //}
                //else
                entity.Id = this.ItemProvider.InsertItem(entity);
            }

            return entity.Id;
        }
    }
}
