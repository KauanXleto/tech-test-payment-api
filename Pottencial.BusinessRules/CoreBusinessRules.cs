﻿using Pottencial.BusinessEntities;
using Pottencial.Provider;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pottencial.BusinessRules
{
    public class CoreBusinessRules
    {
        CoreProvider CoreProvider = new CoreProvider();
        SellerBusinessRules SellerBusinessRules = new SellerBusinessRules();
        ItemBusinessRules ItemBusinessRules = new ItemBusinessRules();

        public IList<SaleStatus> GetSaleStatus()
        {
            return this.CoreProvider.GetSaleStatus();
        }

        public IList<SaleInfo> GetSaleInfo(SaleInfo filter)
        {
            var result = this.CoreProvider.GetSaleInfo(filter);

            if (result != null && result.Count > 0)
            {
                foreach (var item in result)
                {
                    var seller = this.SellerBusinessRules.GetSellers(new Seller() { Id = item.SellerId }).FirstOrDefault();

                    if(seller != null)
                        item.Seller = seller;

                    if (item.Items != null && item.Items.Count > 0)
                    {
                        foreach (var _item in item.Items)
                        {
                            var itemFind = this.ItemBusinessRules.GetItems(new Item() { Id = _item.Id }).FirstOrDefault();

                            if(itemFind != null)
                            {
                                _item.Name = itemFind.Name;
                                _item.Quantity = itemFind.Quantity;
                                _item.UnitValue = itemFind.UnitValue;
                            }
                        }
                    }
                }
            }

            return result;
        }

        public int RegisterSale(SaleInfo entity)
        {

            #region Validações

            #region Validando vendedor

            if (entity.SellerId > 0)
            {
                var findSeller = this.SellerBusinessRules.GetSellers(new Seller() { Id = entity.SellerId }).FirstOrDefault();

                if (findSeller == null || (findSeller != null && findSeller.Id == 0))
                    throw new Exception("Erro!! Vendedor não encontrado");
            }
            else if (entity.Seller != null)
            {
                this.SellerBusinessRules.SaveSeller(entity.Seller, true);

                entity.SellerId = entity.Seller.Id;
            }
            else
                throw new Exception("Erro!! É necessário informar o vendedor");
            #endregion

            #region Validando Itens

            if (entity.Items == null || (entity.Items != null && entity.Items.Count == 0))
                throw new Exception("Erro!! É necessário informar os itens");
            else
            {
                foreach (var item in entity.Items)
                {

                    if (item.Id > 0)
                    {
                        var findItem = this.ItemBusinessRules.GetItems(new Item() { Id = item.Id }).FirstOrDefault();

                        if (findItem == null || (findItem != null && findItem.Id == 0))
                            throw new Exception("Erro!! Item não encontrado");
                    }
                    else
                    {
                        this.ItemBusinessRules.SaveItem(item, true);
                    }
                }
            }
            #endregion

            #endregion

            entity.SaleStatusId = (int)SaleStatus.ESaleStatus.Aguardando_pagamento;

            this.SaveSaleInfo(entity);

            return entity.Id;
        }

        public int UpdateSaleStatus(int SaleId, SaleStatus.ESaleStatus StatusSale)
        {
            if (SaleId <= 0)
                throw new Exception("Erro!! É necessário informar a venda a ser atualizada");

            if (StatusSale <= 0)
                throw new Exception("Erro!! É necessário informar um status válido");

            var saleFind = this.GetSaleInfo(new SaleInfo() { Id = SaleId }).FirstOrDefault();
            var status = this.GetSaleStatus().Where(x => x.Id == (int)StatusSale).FirstOrDefault();

            if (saleFind != null && saleFind.Id > 0)
            {
                if(saleFind.SaleStatusId == (int)StatusSale)
                    throw new Exception($"Erro!! A venda ja se encontra nesse status ({status.Name})");

                if (saleFind.SaleStatusId == (int)SaleStatus.ESaleStatus.Aguardando_pagamento)
                {
                    if(StatusSale != SaleStatus.ESaleStatus.Pagamento_aprovado && StatusSale != SaleStatus.ESaleStatus.Cancelada)
                        throw new Exception($"Erro!! Status ({status.Name}) inválido para a situação da venda -> Status Atual: {saleFind.SaleStatus.Name}");
                }
                else if(saleFind.SaleStatusId == (int)SaleStatus.ESaleStatus.Pagamento_aprovado)
                {
                    if (StatusSale != SaleStatus.ESaleStatus.Enviado_para_transportadora && StatusSale != SaleStatus.ESaleStatus.Cancelada)
                        throw new Exception($"Erro!! Status ({status.Name}) inválido para a situação da venda -> Status Atual: {saleFind.SaleStatus.Name}");
                }
                else if (saleFind.SaleStatusId == (int)SaleStatus.ESaleStatus.Enviado_para_transportadora)
                {
                    if (StatusSale != SaleStatus.ESaleStatus.Entregue)
                        throw new Exception($"Erro!! Status ({status.Name}) inválido para a situação da venda -> Status Atual: {saleFind.SaleStatus.Name}");
                }
                else
                    throw new Exception($"Erro!! Status ({status.Name}) inválido para a situação da venda -> Status Atual: {saleFind.SaleStatus.Name}");

                saleFind.SaleStatusId = (int)StatusSale;

                this.SaveSaleInfo(saleFind);
            }
            else
                throw new Exception("Erro!! Venda não localizada");

            return saleFind.Id;
        }

        public int SaveSaleInfo(SaleInfo entity)
        {
            if (entity.Id > 0)
                this.CoreProvider.UpdateSaleInfo(entity);
            else
                entity.Id = this.CoreProvider.InsertSaleInfo(entity);

            return entity.Id;
        }
    }
}
