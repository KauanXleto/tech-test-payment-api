﻿using Pottencial.BusinessEntities;
using Pottencial.Provider;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Pottencial.BusinessRules
{
    public class SellerBusinessRules
    {
        SellerProvider SellerProvider = new SellerProvider();

        public IList<Seller> GetSellers(Seller filter)
        {
            return this.SellerProvider.GetSellers(filter);
        }
        public int SaveSeller(Seller entity, bool? IfExistReturn = false)
        {
            entity.ExecuteValidation();

            if (entity.Id > 0)
            {
                var findSeller = this.GetSellers(new Seller() { Id = entity.Id }).FirstOrDefault();

                if (findSeller == null || (findSeller != null && findSeller.Id == 0))
                    throw new Exception("Erro!! Vendedor não encontrado");

                this.SellerProvider.UpdateSeller(entity);
            }
            else
            {
                var existSameSeller = this.GetSellers(new Seller()
                {
                    Name = entity.Name,
                    CpfCnpj = entity.CpfCnpj
                }).FirstOrDefault();

                if (existSameSeller != null && existSameSeller.Id > 0)
                {
                    if (IfExistReturn == true)
                        entity = existSameSeller;
                    else
                        throw new Exception("Erro!! Já existe uma pessoa com o mesmo Nome ou CpfCnpj");
                }
                else
                    entity.Id = this.SellerProvider.InsertSeller(entity);

            }

            return entity.Id;
        }
    }
}
