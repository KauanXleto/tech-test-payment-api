﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Pottencial.BusinessEntities
{
    public class SaleStatus
    {
        public int Id { get; set; }
        public string Name { get; set; }

        [JsonIgnore]
        public ESaleStatus eSaleStatus { get; set; }

        public enum ESaleStatus
        {
            Aguardando_pagamento = 1,
            Pagamento_aprovado,
            Enviado_para_transportadora,
            Entregue,
            Cancelada
        }
    }
}
