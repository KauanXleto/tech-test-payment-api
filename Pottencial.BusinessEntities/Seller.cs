﻿using Pottencial.Commun.TextHelper;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Pottencial.BusinessEntities
{
    public class Seller
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CpfCnpj { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }

        public List<string> ExecuteValidation()
        {
            var result = new List<string>();

            if (string.IsNullOrWhiteSpace(this.Name) || this.Name.Length < 3)
                result.Add("Erro!! É necessário informar um nome válido");

            if (!CpfCnpjValidator.IsValid(this.CpfCnpj))
                result.Add("Erro!! É necessário informar um CPJ/CNPJ válido");

            if (string.IsNullOrWhiteSpace(this.Email) || (!string.IsNullOrWhiteSpace(this.Email) && !RegexValidator.IsEmail(this.Email)))
                result.Add("Erro!! É necessário informar um Email válido");

            if (string.IsNullOrWhiteSpace(this.Phone) || (!string.IsNullOrWhiteSpace(this.Phone) && !RegexValidator.IsPhone(this.Phone)))
                result.Add("Erro!! É necessário informar um telefone válido");

            if (result != null && result.Count > 0)
                throw new Exception(String.Join(";", result));

            return result;
        }
    }
}
