﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Pottencial.BusinessEntities
{
    public class SaleInfo
    {
        [JsonIgnore]
        public int Id { get; set; }
        public int SellerId { get; set; }
        [JsonIgnore]
        public int SaleStatusId { get; set; }
        public DateTime DateSale { get; set; }
        public List<Item> Items { get; set; }
        public Seller Seller { get; set; }
        public SaleStatus SaleStatus { get; set; }
    }
}
