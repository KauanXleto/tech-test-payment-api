﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Pottencial.BusinessEntities
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal UnitValue { get; set; }
        public int Quantity { get; set; }

        public List<string> ExecuteValidation()
        {
            var result = new List<string>();

            if (string.IsNullOrWhiteSpace(this.Name) || this.Name.Length < 3)
                result.Add("Erro!! É necessário informar um nome válido. O nome deve conter no mínimo 3 caracteres");

            if (this.UnitValue <= 0)
                result.Add("Erro!! É necessário informar um valor válido");

            if (this.Quantity <= 0)
                result.Add("Erro!! É necessário informar uma quantidade válida");

            if (result != null && result.Count > 0)
                throw new Exception(String.Join(";", result));

            return result;
        }
    }
}
