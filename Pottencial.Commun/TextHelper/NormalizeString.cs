﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Commun.TextHelper
{
    public static class NormalizeString
    {
        public static string StandardizeText(string txt)
        {
            return RemoveAccent(txt.Trim().ToLower());
        }

        public static string RemoveAccent(string txt)
        {
            StringBuilder sbReturn = new StringBuilder();
            var arrayText = txt.Normalize(NormalizationForm.FormD).ToCharArray();
            foreach (char letter in arrayText)
            {
                if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                    sbReturn.Append(letter);
            }

            return sbReturn.ToString();
        }
    }
}
